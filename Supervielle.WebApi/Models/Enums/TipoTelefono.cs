﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Models.Enums
{
    public enum TipoTelefono
    {
        Particular = 0,
        Laboral = 1
    }
}
