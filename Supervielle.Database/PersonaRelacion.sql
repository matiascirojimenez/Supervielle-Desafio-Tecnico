﻿CREATE TABLE [dbo].[PersonaRelacion]
(
	[IdPersona1]	INT			NOT NULL,
	[IdPersona2]	INT			NOT NULL,
	[TipoRelacion]  INT			NOT NULL,
	CONSTRAINT [PersonaRelacion_PK]	PRIMARY KEY CLUSTERED ([IdPersona1] ASC, [IdPersona2] ASC),
	CONSTRAINT [FK_PersonaRelacion_Persona1] FOREIGN KEY ([IdPersona1]) REFERENCES [dbo].[Persona] ([IdPersona]),
	CONSTRAINT [FK_PersonaRelacion_Persona2] FOREIGN KEY ([IdPersona2]) REFERENCES [dbo].[Persona] ([IdPersona]),
)