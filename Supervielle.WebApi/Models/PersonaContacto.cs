﻿using Supervielle.WebApi.Models.Enums;
using System;
using System.Collections.Generic;

#nullable disable

namespace Supervielle.WebApi.Models
{
    public partial class PersonaContacto
    {
        public int IdPersona { get; set; }
        public int Renglon { get; set; }
        public string Numero { get; set; }
        public TipoTelefono TipoTelefono { get; set; }
        public string Descripcion { get; set; }

        public virtual Persona IdPersonaNavigation { get; set; }
    }
}
