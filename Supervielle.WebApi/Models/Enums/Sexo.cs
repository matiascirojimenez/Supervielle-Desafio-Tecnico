﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Models.Enums
{
    //[JsonConverter(typeof(StringEnumConverter))]
    public enum Sexo
    {
        Masculino = 0,
        Femenino = 1
    }
}
