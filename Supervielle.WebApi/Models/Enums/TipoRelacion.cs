﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Models.Enums
{
    public enum TipoRelacion
    {
        Hermano = 0,
        Primo = 1,
        Tio = 2
    }
}
