﻿CREATE TABLE [dbo].[Persona]
(
	[TipoDocumento]			INT				NOT NULL,
	[NumeroDocumento]		INT				NOT NULL,
	[IdPersona]				INT				NOT NULL IDENTITY(1,1),
	[Pais]					VARCHAR(100)	NOT NULL,
	[Sexo]					INT				NOT NULL,
	[Nacionalidad]			VARCHAR(100)	NOT NULL,
	[FechaNacimiento]		DATE			NOT NULL,
	CONSTRAINT [Persona_PK]	PRIMARY KEY CLUSTERED ([IdPersona] ASC),
	CONSTRAINT [Persona_UK_TipoNumeroDocumento] UNIQUE NONCLUSTERED ([TipoDocumento] ASC, [NumeroDocumento] ASC)
)