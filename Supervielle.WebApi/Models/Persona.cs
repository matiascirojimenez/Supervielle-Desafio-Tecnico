﻿using FluentValidation;
using Supervielle.WebApi.Models.Enums;
using System;
using System.Collections.Generic;

#nullable disable

namespace Supervielle.WebApi.Models
{
    public partial class Persona
    {
        public Persona()
        {
            PersonaContactos = new HashSet<PersonaContacto>();
            PersonaRelacionIdPersona1Navigations = new HashSet<PersonaRelacion>();
            PersonaRelacionIdPersona2Navigations = new HashSet<PersonaRelacion>();
        }

        public TipoDocumento TipoDocumento { get; set; }
        public int NumeroDocumento { get; set; }
        public int IdPersona { get; set; }
        public string Pais { get; set; }
        public Sexo Sexo { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime FechaNacimiento { get; set; }

        public virtual ICollection<PersonaContacto> PersonaContactos { get; set; }
        public virtual ICollection<PersonaRelacion> PersonaRelacionIdPersona1Navigations { get; set; }
        public virtual ICollection<PersonaRelacion> PersonaRelacionIdPersona2Navigations { get; set; }
    }

    public class PersonaModelValidator
        : AbstractValidator<Persona>
    {
        public PersonaModelValidator()
        {
            RuleFor(x => x.Pais).NotEmpty().WithMessage("{PropertyName} should be not empty. NEVER!");
            RuleFor(x => x.Nacionalidad).NotEmpty().WithMessage("{PropertyName} should be not empty. NEVER!");
            RuleFor(x => x.FechaNacimiento).NotEqual(DateTime.MinValue);
            RuleFor(x => x.FechaNacimiento).Must(BeAValidAge).WithMessage("No pueden crearse personas menores a 18 años");
        }

        private bool BeAValidAge(DateTime fechaNacimiento)
        {
            var edad = (int)Math.Floor((DateTime.Now - fechaNacimiento).TotalDays / 365);
            return edad >= 18;
        }
    }
}
