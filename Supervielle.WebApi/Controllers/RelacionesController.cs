﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Supervielle.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Controllers
{
    public class RelacionesController : BaseController
    {
        public RelacionesController(SupervielleContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Retorna la relación que existe entre ambas personas.
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <returns>Un string indicando que relacion existe entre ambos ID's</returns>
        /// <response code="200">Un String con el mensaje de la relacion que existe</response>
        /// <response code="400">Si no existe una relacion entre ambos ID's</response>
        [HttpGet("{id1:int}/{id2:int}")]
        public IActionResult Get(int id1, int id2)
        {
            var dbEntity = _context.PersonaRelacions.FirstOrDefault(f => f.IdPersona1 == id1 && f.IdPersona2 == id2);
            if (dbEntity == null)
                throw new Exception("No existe relacion entre ambos ID enviados");

            return Ok($"{id1} es {dbEntity.TipoRelacion} {id2}");
        }
    }
}
