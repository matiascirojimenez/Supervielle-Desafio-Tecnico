﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Models.Enums
{
    public enum TipoDocumento
    {
        CI = 0,
        CUIT,
        PAS,
        LE,
        DNI
    }
}
