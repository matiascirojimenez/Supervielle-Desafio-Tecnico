﻿using Supervielle.WebApi.Models.Enums;
using System;
using System.Collections.Generic;

#nullable disable

namespace Supervielle.WebApi.Models
{
    public partial class PersonaRelacion
    {
        public PersonaRelacion()
        {
            this.TipoRelacion = (TipoRelacion)new Random().Next(0, 1);
        }

        public PersonaRelacion(int id1, int id2)
            : this()
        {
            this.IdPersona1 = id1;
            this.IdPersona2 = id2;
        }

        public int IdPersona1 { get; set; }
        public int IdPersona2 { get; set; }
        public TipoRelacion TipoRelacion { get; set; }

        public virtual Persona IdPersona1Navigation { get; set; }
        public virtual Persona IdPersona2Navigation { get; set; }
    }
}
