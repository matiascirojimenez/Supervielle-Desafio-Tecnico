﻿CREATE TABLE [dbo].[PersonaContacto]
(
	[IdPersona]			INT				NOT NULL,
	[Renglon]			INT				NOT NULL IDENTITY(1,1),
	[Numero]			VARCHAR(80)		NOT NULL,
	[TipoTelefono]		INT				NOT NULL,
	[Descripcion]		VARCHAR(100)	NULL,
	CONSTRAINT [PK__PersonaContacto] PRIMARY KEY CLUSTERED ([IdPersona] ASC, [Renglon] ASC),
	CONSTRAINT [FK_PersonaContacto_Persona] FOREIGN KEY ([IdPersona]) REFERENCES [dbo].[Persona] ([IdPersona])
)