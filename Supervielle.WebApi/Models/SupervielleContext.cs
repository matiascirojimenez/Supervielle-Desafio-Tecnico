﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Supervielle.WebApi.Models
{
    public partial class SupervielleContext : DbContext
    {
        public SupervielleContext()
        {
        }

        public SupervielleContext(DbContextOptions<SupervielleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<PersonaContacto> PersonaContactos { get; set; }
        public virtual DbSet<PersonaRelacion> PersonaRelacions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=aspnetcore");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.HasKey(e => e.IdPersona)
                    .HasName("Persona_PK");

                entity.ToTable("Persona");

                entity.HasIndex(e => new { e.TipoDocumento, e.NumeroDocumento }, "Persona_UK_TipoNumeroDocumento")
                    .IsUnique();

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.Nacionalidad)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pais)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Sexo)
                    .HasConversion<int>();

                entity.Property(e => e.TipoDocumento)
                                    .HasConversion<int>();
            });

            modelBuilder.Entity<PersonaContacto>(entity =>
            {
                entity.HasKey(e => new { e.IdPersona, e.Renglon })
                    .HasName("PK__PersonaContacto");

                entity.ToTable("PersonaContacto");

                entity.Property(e => e.Renglon).ValueGeneratedOnAdd();

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.PersonaContactos)
                    .HasForeignKey(d => d.IdPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonaContacto_Persona");

                entity.Property(e => e.TipoTelefono)
                    .HasConversion<int>();
            });

            modelBuilder.Entity<PersonaRelacion>(entity =>
            {
                entity.HasKey(e => new { e.IdPersona1, e.IdPersona2 })
                    .HasName("PersonaRelacion_PK");

                entity.ToTable("PersonaRelacion");

                entity.HasOne(d => d.IdPersona1Navigation)
                    .WithMany(p => p.PersonaRelacionIdPersona1Navigations)
                    .HasForeignKey(d => d.IdPersona1)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonaRelacion_Persona1");

                entity.HasOne(d => d.IdPersona2Navigation)
                    .WithMany(p => p.PersonaRelacionIdPersona2Navigations)
                    .HasForeignKey(d => d.IdPersona2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonaRelacion_Persona2");

                entity.Property(e => e.TipoRelacion)
                        .HasConversion<int>();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
