﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supervielle.WebApi.Models;
using Supervielle.WebApi.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Controllers
{
    public class EstadisticasController : BaseController
    {
        public EstadisticasController(SupervielleContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Devuelva cifras totalizadoras de cantidad de mujeres, cantidad de hombres, porcentaje de argentinos sobre el total.
        /// </summary>
        /// <returns>Un objecto de la clase EstadisticasDTO</returns>
        /// <response code="200">Retorna una instancia de un data transfer object</response>
        [HttpGet]
        public async Task<ActionResult<EstadisticasDTO>> GetEstadisticas()
        {
            var argentinos = await _context.Personas.CountAsync(c => c.Nacionalidad.ToUpper().Contains("ARG"));
            var total = await _context.Personas.CountAsync();

            return new EstadisticasDTO()
            {
                CantidadHombres = await _context.Personas.CountAsync(c => c.Sexo == Models.Enums.Sexo.Masculino),
                CantidadMujeres = await _context.Personas.CountAsync(c => c.Sexo == Models.Enums.Sexo.Femenino),
                PorcentajeArgentinos = argentinos * 100 / total
            };
        }
    }
}
