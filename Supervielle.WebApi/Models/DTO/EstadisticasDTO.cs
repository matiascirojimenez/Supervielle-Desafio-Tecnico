﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supervielle.WebApi.Models.DTO
{
    public class EstadisticasDTO
    {
        [JsonProperty("cantidad_mujeres")]
        public int CantidadMujeres { get; set; }

        [JsonProperty("cantidad_hombres")]
        public int CantidadHombres { get; set; }

        [JsonProperty("porcentaje_argentinos")]
        public decimal PorcentajeArgentinos { get; set; }
    }
}
