﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Supervielle.WebApi.Models;

namespace Supervielle.WebApi.Controllers
{
    public class PersonasController : BaseController
    {
        public PersonasController(SupervielleContext context)
            : base(context)
        {
        }

        // GET: api/Personas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Persona>>> GetPersonas()
        {
            return await _context.Personas.ToListAsync();
        }

        // GET: api/Personas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Persona>> GetPersona(int id)
        {
            var persona = await _context.Personas.FindAsync(id);

            if (persona == null)
            {
                return NotFound();
            }

            return persona;
        }

        // PUT: api/Personas/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersona(int id, Persona persona)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != persona.IdPersona)
            {
                return BadRequest();
            }

            _context.Entry(persona).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Permite dar de alta un registro en la tabla Persona
        /// </summary>
        /// <remarks>
        /// Ejemplo request:
        ///
        ///     POST /Personas/PostPersona
        ///     {
        ///         "TipoDocumento": 3,
        ///         "NumeroDocumento": 64245666,
        ///         "Pais": "URUGUAY",
        ///         "Sexo": 1,
        ///         "FechaNacimiento": "1989-10-24",
        ///         "Nacionalidad": "Argentino",
        ///         "PersonaContactos": [
        ///             {
        ///                 "Numero": "657887222",
        ///                 "TipoTelefono": 1,
        ///                 "Descripcion": ""
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <param name="persona"></param>
        /// <returns>Un objecto de la clase Persona</returns>
        /// <response code="200">Retorna una persona recien creada</response>
        /// <response code="400">Si hay un error en el request enviado</response>  
        // POST: api/Personas
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754[HttpPost]
        [HttpPost]
        public async Task<ActionResult<Persona>> PostPersona(Persona persona)
        {
            if (persona.PersonaContactos == null || persona.PersonaContactos.Count() == 0)
                throw new Exception("Las personas deben tener al menos un dato de contacto");

            _context.Personas.Add(persona);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersona", new { id = persona.IdPersona }, persona);
        }

        // DELETE: api/Personas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePersona(int id)
        {
            var persona = await _context.Personas.FindAsync(id);
            if (persona == null)
            {
                return NotFound();
            }

            _context.Personas.Remove(persona);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PersonaExists(int id)
        {
            return _context.Personas.Any(e => e.IdPersona == id);
        }

        /// <summary>
        /// Indica que ":id1" es padre de ":id2"
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <returns>Un objecto de la clase Relacion entre ":id1" y ":id2"</returns>
        /// <response code="200">Retorna una relacion recien creada</response>
        /// <response code="400">Si hay un error en el request enviado</response>  
        [HttpPost("{id1:int}/padre/{id2:int}")]
        public async Task<IActionResult> Get(int id1, int id2)
        {
            if (await _context.Personas.CountAsync(c => c.IdPersona == id1) == 0)
                throw new Exception($"No existe la persona para el id1: {id1}");

            if (await _context.Personas.CountAsync(c => c.IdPersona == id2) == 0)
                throw new Exception($"No existe la persona para el id2: {id2}");

            var entity = new PersonaRelacion(id1, id2);
            _context.PersonaRelacions.Add(entity);
            await _context.SaveChangesAsync();
            return Ok($"{id1} es padre de {id2}.");
        }
    }
}
